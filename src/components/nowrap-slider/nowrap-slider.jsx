import './nowrap-slider.less';
import React from 'react';
import ReactDOM from 'react-dom';

export default class NoWrapSlider extends React.Component {
  static propTypes = {
    selector        : React.PropTypes.string,
    nextButton      : React.PropTypes.string,
    prevButton      : React.PropTypes.string,
    delay           : React.PropTypes.number,
    speed           : React.PropTypes.number,
    effect          : React.PropTypes.string,
    animationEffect : React.PropTypes.string,
    slides          : React.PropTypes.array.isRequired
  };

  static defaultProps = {
    selector        : '.js-es-slider',
    nextButton      : '.es-slide-next',
    prevButton      : '.es-slide-prev',
    delay           : 5000,
    speed           : 300,
    effect          : 'slide',
    animationEffect : 'linear'
  };
  state = {
    currentSlide : 0,
    status       : false,
    marginLeft   : 0,
    slides       : [],
    hover        : false
  };
  wrapWidth = 0;

  constructor (props) {
    super(props);
    let key = 0;
    this.state.slides = props.slides.map(element => {
      const item = {
        content : element,
        index   : key
      };
      key++;
      return item;
    });
  }

  nextSlide = () => {
    if (this.state.currentSlide + 1 < this.props.slides.length) {
      this.goTo(this.state.currentSlide + 1);
    } else {
      this.goTo();
    }
  };

  prevSlide = () => {
    if (this.state.currentSlide === 0) {
      this.goTo(this.state.slides.length - 1);
    } else {
      this.goTo(this.state.currentSlide - 1);
    }
  };

  goTo = (index = 0) => {
    index = parseInt(index);
    if (typeof this.props.slides[index] !== 'undefined') {
      const newState = {
        ...this.state, ...{
          marginLeft   : `${(this.wrapWidth * index) * -1}px`,
          currentSlide : index
        }
      };
      this.setState(newState);
    } else {
      console.warn(`The slide with key ${index} is not find`);
    }
  };

  hoverToggle = () => {
    let state = {
      hover: !this.state.hover
    };
    this.setState({...this.state, ...state});
  };

  componentDidMount () {
    this.wrapWidth = ReactDOM.findDOMNode(this).querySelector('.js-es-slider').offsetWidth;
    if (this.props.delay !== 0) {
      setInterval(() => {
        this.nextSlide();
      }, this.props.delay);
    }
  }

  render () {
    return (
      <div className="nowrap-slider-container" onMouseOver={this.hoverToggle} onMouseOut={this.hoverToggle}>
        <ul className="es-slider js-es-slider">
          {this.state.slides.map(slide => {
            return (<li
              className="es-slider__item"
              key={slide.index}
              style={{
                transition : `${this.props.speed}ms ${this.props.animationEffect}`,
                marginLeft : slide.index === 0 ? this.state.marginLeft : 'auto'
              }}
            >{slide.content}</li>);
          })}
        </ul>
        <div className={`nav-slider ${this.state.hover ? 'react-switch' : ''}`}>
          <svg href="javascript:void(0)" className="nav-slider__button nav-slider__button_prev" onClick={this.prevSlide}>
            <symbol id="s-crown">
              <path id="svg-cursor" className="svg-cursor__left"
                    d="M 20,20 L 145,150 L -90,175 z"/>
            </symbol>
            <use href="#s-crown" x="2" y="3"/>
          </svg>
          <svg href="javascript:void(0)" className="nav-slider__button nav-slider__button_next" onClick={this.nextSlide}>
            <symbol id="s-crown1">
              <path id="svg-cursor" className="svg-cursor__right"
                    d="M 20,20 L 145,150 L -90,175 z"/>
            </symbol>
            <use href="#s-crown1" x="2" y="3"/>
          </svg>
        </div>
        <div className="dots-slider">
          {this.state.slides.map(item => {
            return (<a href="javascript:void(0)" className={`dots-slider__item ${item.index === this.state.currentSlide ? 'active' : ''}`} key={item.index} onClick={this.goTo.bind(null, item.index)}>1</a>)
          })}
        </div>
      </div>
    );
  };
}
