import 'css-reset/reset.css';
import React from 'react';
import ReactDom from 'react-dom';
import NoWrapSlider from './components/nowrap-slider/nowrap-slider.jsx';

ReactDom.render((
  <NoWrapSlider
    delay={0}
    slides={[
    (<img src="/img/slider-item1.jpg" alt="" />),
    (<img src="/img/slider-item2.jpg" alt="" />),
    (<img src="/img/slider-item3.jpg" alt="" />),
    (<img src="/img/slider-item4.jpg" alt="" />),
    (<img src="/img/slider-item5.jpg" alt="" />)
  ]}/>
), document.getElementById('root'));
