module.exports = {
  entry: {
    demo  : './src/demo.js',
    index : ['./src/components/nowrap-slider/nowrap-slider.jsx']
  },
  output: {
    path     : './',
    filename : '[name].js',
    library  : '[name]'
  },
  devServer: {
    inline : true,
    host   : '0.0.0.0',
    port   : 8000
  },
  module: {
    loaders: [
      {
        test   : /\.css$/,
        loader : 'style-loader!css-loader'
      },
      {
        test   : /\.less$/,
        loader : 'style-loader!css-loader!less-loader'
      },
      {
        test    : /.jsx?$/,
        loader  : 'babel',
        exclude : /node_modules/,
        query   : {presets: ['es2015', 'react', 'stage-0']}
      },
      {
        test    : /.js?$/,
        loader  : 'babel',
        exclude : /node_modules/,
        query   : {presets: ['es2015', 'react', 'stage-0']}
      },
      {
        test   : /\.svg/,
        loader : 'url-loader'
      }
    ]
  },
  resolve: {
    extensions : ['', '.js', '.jsx'],
    alias      : {
      'config': ''
    }
  }
};
